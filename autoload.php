<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

//(composer-devlinks) -> nunzion -> vendor
require_once __DIR__ . '/../../autoload.php';

$composerLinksDevJson = __DIR__ . "/../../../composer.links.dev.json";
if (file_exists($composerLinksDevJson))
{
    $items = json_decode(file_get_contents($composerLinksDevJson));
    if ($items === null)
        throw new RuntimeException("composer.links.dev.json is not a valid json file!");
    
    foreach ($items->links as $link)
    {
        $autoload = __DIR__ . "/../../../" . $link . "/vendor/autoload.php";
        if (file_exists($autoload))
            require_once $autoload;
    }
}
