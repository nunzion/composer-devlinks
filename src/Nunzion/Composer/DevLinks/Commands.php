<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\Composer\DevLinks;

use Composer\Script\Event;

class Commands
{      
    public static function includeLinkedAutoloaders(Event $event)
    {
        $vendorDir = $event->getComposer()->getConfig()->get("vendor-dir");
        $autoloadfile = $vendorDir . "/autoload.php";
        if (file_exists($autoloadfile))
        {
            $content = str_replace("return ComposerAutoloaderInit", '$result = ComposerAutoloaderInit', 
                    file_get_contents($autoloadfile));
            $content .= "\n";
            
            $composerLinksDevJson = $vendorDir . "/../composer.links.dev.json";
            if (file_exists($composerLinksDevJson))
            {
                $items = json_decode(file_get_contents($composerLinksDevJson));
                if ($items === null)
                    throw new RuntimeException("composer.links.dev.json is not a valid json file!");

                foreach ($items->links as $link)
                {   
                    $content .= self::processLink($link);
                }
            }
            
            $content .= "\nreturn \$result;\n";
            
            file_put_contents($autoloadfile, $content);
        }
    }
    
    private static function processLink($link)
    {
        $autoloadFile = null;

        if (strcmp(substr($link, -strlen(".php")), ".php") === 0)
            $autoloadFile = $link;
        else
            $autoloadFile = $link . "/vendor/autoload.php";

        
        
        if (self::isRelativePath($autoloadFile))
            $path = '__DIR__ . "/../' . $autoloadFile . '"';
        else
            $path = '"' . $autoloadFile . '"';
        

        return "if (file_exists(" . $path . "))\n    require_once(" . $path . ");\n";
    }
    
    private static function isRelativePath($path)
    {
        return preg_match("/^((\\/)|([A-Za-z]\\:))/", $path) === 0;
    }
}
